const Discord = require('discord.js');
const dotenv = require('dotenv');
const path = require('path');

const dadjokeController = require('./controller/dadjoke.controller');
const uptimeController = require('./controller/uptime.controller');
const helpController = require('./controller/help.controller');
const memeController = require('./controller/meme.controller');
const uwuController = require('./controller/uwu.controller');

dotenv.config({ path: path.resolve(__dirname, '.env') });
const discordToken = process.env.DISCORD_TOKEN;
const giphyApiKey = process.env.GIPHY_API_KEY;
const client = new Discord.Client();

if (!discordToken) throw new Error('DISCORD_TOKEN environment variable is missing!');
if (!giphyApiKey) throw new Error('GIPHY_API_KEY environment variable is missing!');

client.on('ready', () => console.log('Discord bot is connected and ready'));

client.on('message', (message) => {
  if (message.content === '!help') helpController.handle(message);
  if (message.content.includes('!uwu')) uwuController.handle(message);
  if (message.content === '!dadjoke') dadjokeController.handle(message);
  if (message.content === '!uptime') uptimeController.handle(message);
  if (message.content.includes('!meme')) memeController.handle(message, giphyApiKey);
});

client
  .login(discordToken)
  .catch((error) => console.log(error.toString()));
