const handle = (message) => {
  message.reply(`Bot process running for \`${process.uptime()} seconds\``);
};

exports.handle = handle;
