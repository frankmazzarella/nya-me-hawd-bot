const request = require('request');

const handle = (message, giphyApiKey) => {
  const searchString = message.cleanContent.split('!meme')[1].trim().replace(/\s+/g, '+');
  const url = `http://api.giphy.com/v1/gifs/search?q=${searchString}&api_key=${giphyApiKey}&limit=1`;
  request.get(url, (error, response, body) => {
    if (error) return message.reply(`ERROR: ${error}`);
    body = JSON.parse(body);
    message.reply(body.data[0].url);
  });
};

exports.handle = handle;
