const handle = (message) => {
  message.reply('You can ask for help with `!help`');
};

exports.handle = handle;
