const request = require('request');

const handle = (message) => {
  const options = { json: true, headers: {'User-Agent': 'cherrikissuwu discord bot'} }
  request.get('https://icanhazdadjoke.com/', options, (error, response, body) => {
    if (error) return message.reply(`ERROR: ${error}`);
    message.reply(body.joke);
  });
};

exports.handle = handle;
